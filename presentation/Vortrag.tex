\documentclass{beamer}
\let\Tiny=\tiny
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[ngerman]{babel} 
\usepackage[T1]{fontenc} 
\usepackage{hyperref}
\usepackage{pgf} 
\usepackage{listings}
\usepackage{pdfpages} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{palatino}
\usepackage{color}
\usepackage[absolute,overlay]{textpos}

\usetheme{progressbar} %Antibes, Frankfurt, PaloAlto, progressbar
%\usefont{structurebold}
%\usefonttheme{structurebold}

%\AtBeginSubsection[]
%{
%	\begin{frame}[t]
%		\frametitle{Agenda}
%		\tableofcontents[sectionstyle=show/hide, %subsectionstyle=show/shaded/hide,subsubsectionstyle=show/show/hide]
%	\end{frame}
%}


\setcounter{tocdepth}{2}

\author{Mirko Köster \and Erik Witt}
\title{Play! Framework}
\subtitle{Play 2.1}
\date{19. Juni 2013}


% "define" Scala
\lstdefinelanguage{scala}{
  morekeywords={abstract,case,catch,class,def,%
    do,else,extends,false,final,finally,%
    for,if,implicit,import,match,mixin,%
    new,null,object,override,package,%
    private,protected,requires,return,sealed,%
    super,this,throw,trait,true,try,%
    type,val,var,while,with,yield},
  otherkeywords={=>,<-,<\%,<:,>:,\#,@},
  sensitive=true,
  morecomment=[l]{//},
  morecomment=[n]{/*}{*/},
  morestring=[b]",
  morestring=[b]',
  morestring=[b]"""
}


\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
 
% Default settings for code listings
\lstset{%frame=tb,
  language=scala,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\ttfamily\footnotesize},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  %frame=single,
  breaklines=true,
  breakatwhitespace=true
  tabsize=3
}  

\setbeamerfont{block}{size=\footnotesize}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\frame{\frametitle{Agenda}\tableofcontents}

\setcounter{tocdepth}{3}

\section{Overview}
\subsection{What are Web-Applications?}
\begin{frame}{What are Web-Applications?}
	\includegraphics[width=0.8\textwidth]{images/ClientServer}\\
	\includegraphics[width=0.8\textwidth]{images/WebApplication}
\end{frame}
\subsection{What is Play?}
\begin{frame}{What is Play?}
	\begin{block}{}
		Play is based on a {\color{red}\textbf{lightweight}}, {\color{red}\textbf{stateless}}, {\color{red}\textbf{web-friendly}} architecture and features predictable and minimal resource consumption (CPU, memory, threads) for {\color{red}\textbf{highly-scalable}} applications.
	\end{block}	
	\begin{block}{Features}
		\begin{itemize}
			\item Development loop (hit refresh workflow)
			\item Type safety
			\item Asynchronous Results
			\item RESTful by default (Http aware)
		\end{itemize}
	\end{block}
\end{frame}

\section{Example-Application}
{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/ApplicationFlow}}
\begin{frame}[plain]
\end{frame}
}
{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/IndexFlow}}
\begin{frame}[plain]
\end{frame}
}
{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/SearchFlow}}
\begin{frame}[plain]
\end{frame}
}

\section{Concepts in Detail}
\subsection{Http-Awareness}
\begin{frame}[fragile]{Http-Awareness}
	Http awareness through explicit request methods in the routes-file and explicit use of status codes as controller results.
	\begin{block}{Explicit Request-Methods}
		GET, POST, PUT, DELETE, HEAD
	\end{block}
	\begin{block}{Explicit Statuscodes}
		\begin{lstlisting}
		Ok(...)	                         // Statuscode 200
		MovedPermanently                 // Statuscode 301
		BadRequest(...)                  // Statuscode 400
		NotFound(...)                    // Statuscode 404
		InternalServerError(...)       // Statuscode 500
		                                 // etc.
		\end{lstlisting}
	\end{block}
\end{frame}
\subsection{Url Routing}
\begin{frame}[fragile]{Url Routing}
	\begin{block}{Variables in routes file}
		\begin{lstlisting}
			// Type-safe variables in path
			GET /user/:userName/posts/:id controller.App.getPost(userName: String, id: Long) 
			
			// Variables with RegEx even more flexible
			GET /user/:name<[a-z]+> controller.App.getUser(name)
			
			// Variables through ?-syntax: /clients?page=2
			GET /clients controller.App.list(page)
		\end{lstlisting}
	\end{block}
\end{frame}
\begin{frame}[fragile]{Url Routing}
	\begin{block}{Variables in routes file}
		\begin{lstlisting}
			// Fixed parameter for method reuse
			GET /          controller.App.show(page = "home")
			GET /:page     controller.App.show(page)
			
			// Default values used if value not specified
			GET /clients controllers.App.list(page: Int ?= 1)
			
			// Optional values, None as default: /api/list?v=2.1.1
			GET /api/list controller.App.list(Option[v])
		\end{lstlisting}
	\end{block}
\end{frame}

\subsection{Json}
\begin{frame}{Json - a first class citizen}
	\begin{block}{Conversion}
		Conversion from and to objects through \lstinline$Reads[T]$ and \lstinline$Writes[T]$ as implicits.
	\end{block}
	\begin{block}{Querying}
		Querying just like navigating through XML documents. Similar to XPath.
	\end{block}
	\begin{block}{Errorhandling}
		Errorhandling through validation. Provides \lstinline$JsSuccess$ or \lstinline$JsFailure$.
	\end{block} 
\end{frame}
\subsection{Async Results}
\begin{frame}{Async Results}
	\begin{alertblock}{Why?}
		If the result of a request is computed by other services, the controller should not block resources while waiting.
	\end{alertblock}
	\begin{block}{How?}
		The controller hands a Future[Result] to the play framework which will serve the result as soon as its promise is redeemed. 
	\end{block}
	\begin{exampleblock}{Consequence!}
		Non-blocking request processing. Way more request throughput.
	\end{exampleblock}
\end{frame}
\subsection{Database}
\begin{frame}{Database / Persistence layer}
	\vspace{-1em}
	\begin{alertblock}{Sql data access}
		The play framework provides the type safe query and access libraries Anorm and Slick. Where Anorm uses sql syntax explicitly, the syntax in Slick is more scala like using for-comprehensions.
	\end{alertblock}
	\begin{block}{Database evolutions}
		Additionally play provides evolution scripts to keep track of your database schema changes.
		\end{block}
	\begin{block}{No-Sql}
		There is no direct integration of no-sql databases but a lot of third-party libraries support.
	\end{block}
\end{frame}
\subsection{more Features}
\begin{frame}{more Features}
	\begin{block}{Forms}
		The play framework provides several helpers to create Html forms and to handle HTTP form data submission and validation.
	\end{block}
	\begin{block}{Reverse Routing}
		Instead of writing urls directly in code you can use the (compiler checked) reverse routing.
	\end{block}
	\begin{block}{Internationalization (I18n)}
		The play framework supports I18n with its \lstinline$Messages$ object and corresponding message files.
	\end{block}
\end{frame}



\section{References}
\begin{frame}{Technology stack}
	\begin{block}{Features}
		\begin{itemize}
			\item Java- \& Scala-API
			\item Built on Akka and SBT
			\item Asset Compiler for CoffeeScript, LESS, etc.
			\item JSON as first class citizen
			\item Persistence / Database access (Anorm, Slick)
			\item Built in testing tools (specs2)			
		\end{itemize}
	\end{block}
\end{frame}

{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{images/wordcloud}}
\begin{frame}[plain]
\end{frame}
}

\begin{frame}{Sources and further reading}	
	\tiny{
	HTTP \& Routing\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaRouting}\\
	\url{http://www.playframework.com/documentation/api/2.1.1/scala/index.html\#play.api.mvc.Results$}\\
	\url{http://de.wikipedia.org/wiki/HTTP-Statuscode}\\
	\url{https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol\#Request_methods}}\\
	
	RESTful\\
	\url{http://de.wikipedia.org/wiki/Representational_State_Transfer}\\
	
	MVC\\
	\url{http://de.wikipedia.org/wiki/Model_View_Controller}\\
	
	Play framework\\
	\url{http://www.playframework.com/}\\
	\url{http://www.playframework.com/documentation/2.1.1/Home}\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaTodoList}\\

	\url{http://vimeo.com/65556263}\\
	\url{https://www.youtube.com/watch?v=pGZkmL_v1Ns}\\
	
	Async\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaAsync}\\
	
	JSON\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaJson}\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaJsonCombinators}\\
	
	Databases\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaAnorm}\\
	\url{http://slick.typesafe.com/}\\
	\url{http://www.playframework.com/documentation/2.1.1/Evolutions}
\end{frame}
\begin{frame}{Sources and further reading}	
	\tiny{
	Views / Template engine\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaTemplates}
	
	Forms\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaForms}\\
	
	Internationalization (I18n)\\
	\url{http://www.playframework.com/documentation/2.1.1/ScalaI18N}
	
	Technologies\\
	\url{http://www.scala-sbt.org/}\\
	\url{http://akka.io/}\\
	\url{http://coffeescript.org/}\\
	\url{http://lesscss.org/}\\
	\url{http://etorreborre.github.io/specs2/}\\}
	
	Link to Example-Application
	\url{https://bitbucket.org/ErikWitt/hawscalaplaypresentation}
\end{frame}


\end{document}
