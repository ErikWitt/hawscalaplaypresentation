package models

import play.api.data.Forms._
import play.api.data.Form

case class Query(query: String)

object Query {
  val queryForm: Form[Query] = Form(
    mapping(
      "query" -> nonEmptyText
    )(Query.apply)(Query.unapply)
  )
}