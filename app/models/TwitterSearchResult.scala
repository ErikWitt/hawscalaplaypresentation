package models

import play.api.libs.json.Reads

case class TwitterResult(title: String, url: String) extends SearchResult

object TwitterResult {

  implicit val twitterResultReader = Reads[TwitterResult] { json =>
    for {
      title <- (json \ "text").validate[String]
      userId <- (json \ "from_user_id_str").validate[String]
      tweetId <- (json \ "id_str").validate[String]
    } yield TwitterResult(title, s"http://twitter.com/$userId/status/$tweetId")
  }

  implicit val twitterResultsReader = Reads[List[SearchResult]] { json =>
    for {
      list <- (json \ "results").validate[List[TwitterResult]]
    } yield list
  }

}