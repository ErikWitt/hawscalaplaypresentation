package services

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import models.SearchResult
import play.api.libs.json.{JsError, JsSuccess, Json, Reads}
import play.api.libs.ws.WS
import play.api.Logger
import scala.util.control.NonFatal
import java.net.URLEncoder

object SearchService {

  def search(url: String)(implicit reads: Reads[List[SearchResult]]): Future[List[SearchResult]] = {
    val responseFuture = WS.url(url).withTimeout(5000).get()
    responseFuture map { response =>
      if (response.status == 200) {
        Json.fromJson(response.json) match {
          case JsSuccess(results, _) => results
          case JsError(_) => {
            Logger.error(s"json response could not be validated: '${response.json}' for query '$url'")
            Nil
          }
        }
      } else {
        Logger.error(s"response code is '${response.status}' for query '$url'")
        Nil //TODO better error handling (Try?)
      }
    } recover {
      case NonFatal(e) => {
        Logger.error(s"Failed Future for query: '$url'", e)
        Nil
      }
    }
  }

  def buildGoogleUrl(query: String) =
    s"https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=${URLEncoder.encode(query, "UTF-8")}"

  def buildTwitterUrl(query: String) =
    s"http://search.twitter.com/search.json?q=${URLEncoder.encode(query, "UTF-8")}"
}
