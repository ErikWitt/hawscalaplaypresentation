package controllers

import play.api.mvc._
import play.api.templates.Html
import services.SearchService
import models.{TwitterResult, GoogleResult, SearchResult}
import scala.concurrent.ExecutionContext.Implicits.global
import  models.Query.queryForm
import play.api.libs.concurrent.Promise

object Application extends Controller {

  def index = Action {
    Ok(views.html.index(queryForm))
  }

  def search = Action {
    implicit request =>
      queryForm.bindFromRequest().fold(
        formWithErrors => BadRequest(views.html.index(queryForm)),
        success => {
          val query = success.query
          import SearchService.{search => performSearch, _}

          val googleResultsFuture = performSearch(buildGoogleUrl(query))(GoogleResult.googleResultsReader)
          val twitterResultsFuture = performSearch(buildTwitterUrl(query))(TwitterResult.twitterResultsReader)

          val htmlFuture = for {
            googleResults <- googleResultsFuture
            twitterResults <- twitterResultsFuture
          } yield {
            buildSearchResultsAsHtml("Google", googleResults) +=
              buildSearchResultsAsHtml("Twitter", twitterResults)
          }

          Async {
            htmlFuture map {html =>
              Ok(views.html.results(html))
            }
          }
        }
      )
  }


  def buildSearchResultsAsHtml(searchProvider: String, searchResults: List[SearchResult]) = {
    val formattedResults = searchResults.map(_.asHtmlLink).mkString("<p>", "<br />", "</p>")

    Html(s"<h2>$searchProvider results:</h2>$formattedResults")
  }

  // ------------------------------------------------------------------------------------------

  val promise = Promise[Long]()

  def async = Action {
    Ok(views.html.async())
  }

  def waiting = Action {
    AsyncResult(promise.future.map((i: Long) => Ok(i.toString)))
  }

  def redeem(i: Long) = Action {
    promise.success(i)
    Ok("Thanks")
  }
}